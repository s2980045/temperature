package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    HashMap<String,Double> bookPrices;

    public Quoter() {
        bookPrices = new HashMap<>();
        initialise();
    }

    public void initialise() {
        bookPrices.put("1", 10.0);
        bookPrices.put("2", 45.0);
        bookPrices.put("3", 20.0);
        bookPrices.put("4", 35.0);
        bookPrices.put("5", 50.0);
        bookPrices.put("others", 0.0);
    }

    public double getFahrenheit(String celsius) {
        return (Double.parseDouble(celsius) * 9/5) + 32;
    }

    public double getBookPrice(String isbn) {
        return bookPrices.get(isbn);
    }
}
